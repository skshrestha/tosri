import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ApplicationConfig, BaseSetting} from './app-config.model';
import {catchError, flatMap, map} from 'rxjs/operators';
import {of} from 'rxjs';

@Injectable()
export class AppConfig {
    static baseSettings: BaseSetting;
    static appConfig: any;

    constructor(private http: HttpClient) {}
    load() {
        const jsonFile = 'assets/config/config.json';
        const appConfigOverride = 'assets/config/override/app_config.json';
        const defaultConfig$ = this.http.get(jsonFile).pipe(
            flatMap((response: ApplicationConfig) => {
                AppConfig.baseSettings = response.base_settings;
                AppConfig.appConfig = response.app_config;

                return this.http.get(appConfigOverride).pipe(
                    map((overrideConfig: ApplicationConfig) => {
                        AppConfig.appConfig = {...response.app_config, ...overrideConfig.app_config};
                        console.log('Merged with app config override file');
                        return of();
                    }),
                    catchError(_ => {
                        console.log(`Did not merge with app config override file`);
                        console.log(_);
                        return of();
                    })
                );
            })
        );
        return defaultConfig$.toPromise();
    }
}
