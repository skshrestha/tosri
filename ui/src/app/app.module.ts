import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app.routing.module';
import {CommonHttpInterceptor} from './core/services/interceptors/common-http-interceptor';
import {AppHeaderComponent} from './navigation/app-header/app-header.component';
import {SideBarComponent} from './navigation/side-bar/side-bar.component';
import {LoginComponent} from './login/login.component';
import {ModalComponent} from './widgets/modal/modal.component';
import {DropDownComponent} from './widgets/drop-down/drop-down.component';
import {MatChipsModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, MatSortModule, MatTableModule, MatButtonModule, MatProgressBarModule, MatTreeModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CookieService} from 'ngx-cookie-service';
import {NotificationService} from './core/services/notification.service';
import {NotificationComponent} from './widgets/notification/notification.component';
import {LoadingTagComponent} from './widgets/loading-tag/loading-tag.component';
import {AppConfig} from './app.config';
import {SafePipe} from './safe.pipe';
import {ChartsModule} from 'ng2-charts';
import {GenericFormComponent} from './widgets/generic-form/generic-form.component';
import {FullCalendarModule} from '@fullcalendar/angular';

import {DashboardComponent} from './components/dashboard/dashboard.component';

export function initializeApp(appConfig: AppConfig) {
    return () => appConfig.load();
}

@NgModule({
    declarations: [
        DashboardComponent,
        AppComponent,
        AppHeaderComponent,
        SideBarComponent,
        LoginComponent,
        DropDownComponent,
        NotificationComponent,
        LoadingTagComponent,
        ModalComponent,
        SafePipe,
        GenericFormComponent
    ],
    imports: [
        FullCalendarModule,
        BrowserModule,
        HttpClientModule,
        ReactiveFormsModule,
        CommonModule,
        AppRoutingModule,
        MatProgressSpinnerModule,
        MatButtonModule,
        MatProgressBarModule,
        MatFormFieldModule,
        MatIconModule,
        MatTableModule,
        MatInputModule,
        MatTreeModule,
        MatPaginatorModule,
        MatSortModule,
        MatChipsModule,
        BrowserAnimationsModule,
        ChartsModule,
        NgbModule
    ],
    exports: [
        NotificationComponent,
        SideBarComponent
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: CommonHttpInterceptor, multi: true },
        CookieService,
        NotificationService,
        HttpClient,
        AppConfig,
        { provide: APP_INITIALIZER, useFactory: initializeApp, deps: [AppConfig], multi: true }
    ],
    entryComponents: [
        ModalComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
