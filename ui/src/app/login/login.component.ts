import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UserInfo, UserSessionDetail} from '../core/models/user-info';
import {AuthenticationService} from '../core/services/authentication.service';
import {AppConfig} from '../app.config';
import {CssModel} from '../navigation/models/css-model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public loading: boolean;
  public loginEnabled: boolean;

  public appName: string = AppConfig.baseSettings.app_name;

  constructor(private fb: FormBuilder, private authenticationService: AuthenticationService, private router: Router) {
      this.loginEnabled = true;
  }

  ngOnInit() {
      this.loading = false;
      this.loginForm = this.fb.group({
          userId: [ this.authenticationService.getRememberedUser(), Validators.required ],
          password: [ '', Validators.required ],
          rememberMe: [ this.authenticationService.getRememberedUser()]
      });
  }

  submit() {
      const formValue = this.loginForm.value;
      this.loading = true;
      const userInfo: UserInfo = new UserInfo(formValue.userId, formValue.password);
      const loginStatus$ = this.authenticationService.login(userInfo, formValue.rememberMe);
      loginStatus$.subscribe((userDetail: UserSessionDetail) => {
          this.loading = false;
          this.router.navigate(['']);
      }, e => {
          this.loading = false;
      });
  }

  get userId(): AbstractControl {
      return this.loginForm.controls['userId'];
  }

  get password(): AbstractControl {
      return this.loginForm.controls['password'];
  }

  get rememberMe(): AbstractControl {
      return this.loginForm.controls['rememberMe'];
  }

  forgot() {
      this.loginEnabled = false;
  }

    public getBodyStyle() {
        const bodyCss: CssModel = AppConfig.appConfig.header_css;
        const style = {};

        if (bodyCss && bodyCss.font_color) {
            style['color'] = bodyCss.font_color;
        }
        if (bodyCss && bodyCss.bg_color) {
            style['background'] = bodyCss.bg_color;
        }

        return style;
    }

}
