import {Route} from '@angular/router';
import {AnonymousUserGuard} from './core/guards/anonymous-user-guard.service';
import {LoginComponent} from './login/login.component';
import {FRONTEND_ENDPOINTS} from './constants';
import {AnyUserGuard} from './core/guards/any-user-guard.service';

import {DashboardComponent} from './components/dashboard/dashboard.component';

export const routes: Route[] = [
    {
        path: FRONTEND_ENDPOINTS.login,
        component: LoginComponent,
        canActivate: [AnonymousUserGuard]
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [
            AnyUserGuard
        ]
    },
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
    },
    {
        path: '**',
        redirectTo: 'dashboard'
    }
];


