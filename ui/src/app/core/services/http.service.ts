import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class HttpService {

    constructor(private http: HttpClient) {
    }

    get<T>(url: string): Observable<T> {
        return this.http.get<{}>(url, {observe: 'response'}).pipe(
            map(resp => resp.body),
            map((responseBody: {data: T}) => responseBody.data)
        );
    }

    post<T>(url: string, data: any): Observable<T> {
        return this.http.post<{}>(url, data, {observe: 'response'}).pipe(
            map(resp => resp.body),
            map( (respBody: {data: T}) => respBody.data)
        );
    }
}
