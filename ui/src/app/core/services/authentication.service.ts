import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {UserInfo, UserSessionDetail} from '../models/user-info';
import {tap} from 'rxjs/operators';
import {CookieService} from 'ngx-cookie-service';
import {PathGeneratorService} from '../utils/path-generator.service';
import {Observable} from 'rxjs';
import {HttpService} from './http.service';
import {AppConfig} from '../../app.config';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    private userKey: string = AppConfig.baseSettings.app_name.toLowerCase().replace(/\s+/, '-') + '-user';

    constructor(private http: HttpClient, private cookieService: CookieService, private pathGenerator: PathGeneratorService,
                private httpService: HttpService) {
    }

    login(userInfo: UserInfo, rememberUser: boolean): Observable<UserSessionDetail> {
        localStorage.setItem(this.userKey, rememberUser ? userInfo.username : '');

        return this.httpService.post<UserSessionDetail>(this.pathGenerator.getLoginPath(), userInfo).pipe(
            tap(session => this.cookieService.set(this.userKey, session.username, new Date(session.expiryMs), '/')
        ));
    }

    logout() {
        this.cookieService.delete(this.userKey, '/');
        return this.http.get(this.pathGenerator.getLogoutPath(), {});
    }

    getRememberedUser() {
        return localStorage.getItem(this.userKey);
    }

    get userid(): string {
        return this.cookieService.get(this.userKey);
    }

    get authenticated(): boolean {
        return this.cookieService.check(this.userKey);
    }
}
