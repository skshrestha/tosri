import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {NotificationService} from '../notification.service';

@Injectable()
export class CommonHttpInterceptor implements HttpInterceptor {

    constructor(private notificationService: NotificationService) {
    }

    private cloneRequest(req: HttpRequest<any>): HttpRequest<any> {
        return req.clone({
            headers: req.headers.set('X-Frame-Options', 'SAMEORIGIN'), // for security
            withCredentials: true
        });
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        req = this.cloneRequest(req);
        return next.handle(req).pipe(
            catchError(error => {
                if (error.error && error.error.message) {
                    this.notificationService.error(error.error.message);
                } else {
                    this.notificationService.error('Internal Server Error');
                }
                return throwError(error);
            })
        );
    }
}
