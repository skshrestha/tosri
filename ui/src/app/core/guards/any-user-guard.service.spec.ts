import { TestBed, inject } from '@angular/core/testing';
import { AnyUserGuard } from './any-user-guard.service';



describe('AnyUserGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AnyUserGuard]
    });
  });

  it('should be created', inject([AnyUserGuard], (service: AnyUserGuard) => {
    expect(service).toBeTruthy();
  }));
});
