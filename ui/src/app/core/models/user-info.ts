export class UserInfo {
    constructor(public username: string, public password: string) {}
}

export class UserSessionDetail {
    constructor(public username: string, public expiryMs: number) {}
}
