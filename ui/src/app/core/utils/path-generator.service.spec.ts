import { TestBed, inject } from '@angular/core/testing';

import { PathGeneratorService } from './path-generator.service';

describe('PathGeneratorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PathGeneratorService]
    });
  });

  it('should be created', inject([PathGeneratorService], (service: PathGeneratorService) => {
    expect(service).toBeTruthy();
  }));
});
