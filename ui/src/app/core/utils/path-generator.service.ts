import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class PathGeneratorService {

    private basePath = 'api';

    // Login Paths
    private authPath = '/auth';
    private loginPath = '/login';
    private logoutPath = '/logout';

    constructor() {
    }

    getAuthPath(): string {
        return this.basePath + this.authPath;
    }

    getLoginPath(): string {
        return this.getAuthPath() + this.loginPath;
    }

    getLogoutPath(): string {
        return this.getAuthPath() + this.logoutPath;
    }
}
