import {NavigationItem} from "../models/navigation_models";

export const menuItems: NavigationItem[] = [
    {
        "name": "Home",
        "link": "/dashboard",
        "requiresLogin": false,
        "iconClass": "fas fa-home"
    },
    {
        "name": "Historical",
        "link": "/historical",
        "requiresLogin": false,
        "iconClass": "fas fa-history"
    }
];
