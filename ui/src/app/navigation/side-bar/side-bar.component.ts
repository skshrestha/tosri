import {Component} from '@angular/core';
import {menuItems} from './menu';
import {NavigationItem} from '../models/navigation_models';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../core/services/authentication.service';

@Component({
    selector: 'app-side-bar',
    templateUrl: './side-bar.component.html',
    styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent {

    constructor(private router: Router, private authService: AuthenticationService) {
    }

    public getMenuItems(): NavigationItem[] {
        if (this.authService.authenticated) {
            return menuItems;
        } else {
            return menuItems.filter(m => !m.requiresLogin);
        }
    }

    public navigateToLink(menuItem: NavigationItem) {
        console.log(menuItem);
        this.router.navigate([menuItem.link]);
    }

    public getMenuStyle() {
        /*const menuCss: CssModel =  AppConfig.appConfig.menu_css;
        const style = {};
        if (menuCss && menuCss.bg_color) {
            style['background'] = menuCss.bg_color;
        }
        if (menuCss && menuCss.font_color) {
            style['color'] = menuCss.font_color;
        }
        return style;*/
    }
}
