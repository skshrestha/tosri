import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {take} from 'rxjs/internal/operators';
import {AuthenticationService} from '../../core/services/authentication.service';
import {AppConfig} from '../../app.config';
import {CssModel} from '../models/css-model';

@Component({
    selector: 'app-header',
    templateUrl: './app-header.component.html',
    styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent implements OnInit {
    public loggedInUser: string;
    public appName: string = AppConfig.baseSettings.app_name;

    constructor(private authenticationService: AuthenticationService, private router: Router) {
    }

    ngOnInit() {
        this.loggedInUser = this.authenticationService.userid;
    }

    isLoggedIn(): boolean {
        return this.authenticationService.authenticated;
    }

    logout() {
        this.authenticationService.logout().pipe(take(1)).subscribe(s => {
            sessionStorage.clear();
            return this.router.navigate(['login']);
        });
    }

    getHeaderStyle() {
        const headerCss: CssModel = AppConfig.appConfig.header_css;
        const style = {};

        if (headerCss &&  headerCss.bg_color) {
            style['background'] = headerCss.bg_color;
        }
        if (headerCss && headerCss.font_color) {
            style['color'] = headerCss.font_color;
        }
        return style;
    }
}
