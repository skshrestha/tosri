export interface NavigationItem {
    name: string;
    link: string;
    requiresLogin: boolean;
    iconClass?: string;
}
