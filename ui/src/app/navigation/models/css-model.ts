export interface CssModel {
    bg_color: string;
    font_color: string;

    active_color: string;
    active_font_color: string;
}
