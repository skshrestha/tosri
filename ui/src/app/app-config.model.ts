export interface ApplicationConfig {
    base_settings: BaseSetting;
    app_config: any;
}

export interface BaseSetting {
    plugins: string[];
    app_name: string;
    default_path: string;
}
