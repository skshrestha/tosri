import {Component, OnInit} from '@angular/core';
import {TosriComponentHttpClientService} from "../../services/http-client.service";
import * as moment from 'moment';
import {flatMap, map} from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    public loading: boolean;
    public streams: any;


    constructor(private httpService: TosriComponentHttpClientService, private activeRoute: ActivatedRoute) {
        this.loading = false;
        this.streams = undefined;

    }

    ngOnInit() {
        this.loading = true;
        /*this.httpService.get('/streams').subscribe(
            result => {
                this.loading = false;
                this.streams = result;
                this.counter = 0;
                this.pipelines = this.streams.data[this.counter].pipelines;
            },
            error => {
                this.loading = false;
            }
        )*/
    }

    activate(stream: any, index: number) {
        this.loading = true;
        this.loading = false
    }

}
