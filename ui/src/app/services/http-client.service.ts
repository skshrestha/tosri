import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpService} from '../core/services/http.service';

@Injectable({
    providedIn: 'root'
})
export class TosriComponentHttpClientService {

    private urlPrefix = 'api';

    constructor(private httpService: HttpService) {
    }

    get<T>(url: string): Observable<T> {
        return this.httpService.get<T>(this.urlPrefix + url);
    }

    post<T>(url: string, data: any): Observable<T> {
        return this.httpService.post<T>(this.urlPrefix + url, data);
    }
}
