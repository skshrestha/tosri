import { TestBed, inject } from '@angular/core/testing';

import { TosriComponentHttpClientService } from './http-client.service';

describe('KabukiJobComponentHttpClientService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TosriComponentHttpClientService]
    });
  });

  it('should be created', inject([TosriComponentHttpClientService], (service: TosriComponentHttpClientService) => {
    expect(service).toBeTruthy();
  }));
});
