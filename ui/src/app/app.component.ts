import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from './core/services/authentication.service';
import {menuItems} from './navigation/side-bar/menu';
import {Title} from '@angular/platform-browser';
import {AppConfig} from './app.config';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {distinctUntilChanged, filter, map} from 'rxjs/operators';
import {FRONTEND_ENDPOINTS} from './constants';
import {AnyUserGuard} from './core/guards/any-user-guard.service';
import {CssModel} from './navigation/models/css-model';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    public redirectToLoginPage: boolean;

    constructor(private authenticationService: AuthenticationService, private titleService: Title,
                private activatedRoute: ActivatedRoute, private router: Router) {
        this.titleService.setTitle(AppConfig.baseSettings.app_name);
    }


    showSideMenu(): boolean {
        return menuItems.length > 0;
    }

    ngOnInit() {
        this.redirectToLoginPage = true;
        this.router.events.pipe(
            filter(event => event instanceof NavigationEnd),
            distinctUntilChanged(),
            map(event => !this.authenticationService.authenticated && this.currPageRequiresLogin(this.activatedRoute.root))
        ).subscribe(event => {
            this.redirectToLoginPage = event;
        });
    }

    private currPageRequiresLogin(route: ActivatedRoute): boolean {
        return route.firstChild.routeConfig.path === FRONTEND_ENDPOINTS.login ||
            (route.firstChild.routeConfig.canActivate || []).findIndex(g => g === AnyUserGuard) === -1;
    }

    public getBodyStyle() {
       /* const bodyCss: CssModel = AppConfig.appConfig.body_css;
        const style = {};

        if (bodyCss && bodyCss.font_color) {
            style['color'] = bodyCss.font_color;
        }
        if (bodyCss && bodyCss.bg_color) {
            style['background'] = bodyCss.bg_color;
        }

        return style;*/
    }
}
