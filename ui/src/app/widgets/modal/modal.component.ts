import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Input() content: ModalContent;

  constructor(public activeModal: NgbActiveModal) {

  }

  ngOnInit() {
  }

  closeModal() {
      this.activeModal.close();
  }
}

export interface ModalContent {
  title?: string;
  url?: string;
  html?: string;
  text?: string;
  object?: any;
}

