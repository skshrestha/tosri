import {Component} from '@angular/core';

/**
 *  Example Usage:
 *  <div *ngIf="loading"><loading></loading></div>
 */
@Component({
    selector: 'loading',
    template: `
        <div class="loading-dots">
            <div class="loading-dots--dot"></div>
            <div class="loading-dots--dot"></div>
            <div class="loading-dots--dot"></div>
        </div>`,
    styleUrls: ['./loading-tag.component.scss']
})
export class LoadingTagComponent  {
}
