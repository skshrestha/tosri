import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { merge, Observable, Subject } from 'rxjs/index';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/internal/operators';

/**
 *  Example Usage:
 *
 <app-drop-down [controlGroup]="accessDetailForm" [control]="resources" [controlName]="'resources'"
 [placeHolderText]="''" [options]="unselectedResources()" [hideSelectedValue]="true" (ItemSelected)="addResource($event)"></app-drop-down>

 */

@Component({
    selector: 'app-drop-down',
    templateUrl: './drop-down.component.html',
    styleUrls: [ './drop-down.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DropDownComponent implements OnInit {

    constructor() {
    }

    @ViewChild('instance', { static: true }) instance: NgbTypeahead;
    @Input() placeHolderText: string;
    @Input() options: string[];
    @Input() controlGroup: FormGroup;
    @Input() controlName: string;
    @Input() control: FormControl;
    @Input() hideSelectedValue: boolean;
    @Output() ItemSelected = new EventEmitter<string>();

    focus$ = new Subject<string>();
    click$ = new Subject<string>();


    ngOnInit() {
    }

    search = (text$: Observable<string>) => {
        const debounced$ = text$.pipe(debounceTime(200), distinctUntilChanged());
        const click$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
        const focus$ = this.focus$;
        const autocomplete$ = merge(debounced$, focus$, click$);
        return autocomplete$.pipe(
            map(term => term === '' ? this.options : this.options.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1))
        );
    };

    itemSelected(value) {
        this.ItemSelected.emit(value.item);
        if (this.hideSelectedValue) {
            value.preventDefault();
        }
    }
}
