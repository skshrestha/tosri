import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpService} from '../../core/services/http.service';

@Component({
    selector: 'app-generic-form',
    templateUrl: './generic-form.component.html',
    styleUrls: ['./generic-form.component.scss']
})
export class GenericFormComponent implements OnInit {

    @Input()
    public plugin: string;

    @Input()
    public formName: string;

    @Input()
    public disableSubmit: boolean;

    @Output() submitForm = new EventEmitter<{ formData: any, formDefinition: any }>();

    public form: FormGroup;
    public formDefinitions: FormDefinition[];

    constructor(private fb: FormBuilder, private httpService: HttpService) {
    }

    ngOnInit() {
        this.disableSubmit = false;
        this.createForm();
    }

    createForm() {
        const controlsConfig: any = {};
        this.httpService.get<FormDefinition[]>(`api/config/request-definitions/${this.plugin}/${this.formName}`).subscribe(
            result => {
                result.forEach(formValue => {
                    controlsConfig[formValue.key] = [formValue.defaultValue];
                    const formRequirments = formValue.requirements;
                    const validators = [
                        formRequirments.required ? Validators.required : undefined,
                        formRequirments.pattern ? Validators.pattern(formRequirments.pattern) : undefined,
                        formRequirments.min && formValue.type === 'number' ? Validators.min(formRequirments.min) : undefined,
                        formRequirments.min && formValue.type !== 'number' ? Validators.minLength(formRequirments.min) : undefined,
                        formRequirments.max && formValue.type === 'number' ? Validators.max(formRequirments.max) : undefined,
                        formRequirments.max && formValue.type !== 'number' ? Validators.maxLength(formRequirments.max) : undefined
                    ].filter(v => v);
                    (controlsConfig[formValue.key] as any[]).push(validators);

                });
                this.form = this.fb.group(controlsConfig);

                this.formDefinitions = result;
            }
        );
    }

    getFormControl(key: string) {
        return this.form.controls[key];
    }

    clickSubmit() {
        this.disableSubmit = true;
        this.submitForm.emit({formData: this.form.getRawValue(), formDefinition: this.formDefinitions});
    }

}

export class FormDefinition {
    constructor(public label: string, public key: string, public type: string, public defaultValue: any,
                public requirements: FormRequirements) {
    }
}

export class FormRequirements {
    constructor(public required: boolean, public pattern?: string, public min?: number, public max?: number, public options?: any[]) {
    }
}

