import {Component, OnInit} from '@angular/core';
import {NotificationService} from '../../core/services/notification.service';
import {NotificationType, Notification} from '../../core/models/notifications';

/**
 *  This is used through NotificationService in ../../core/services/notification.service
 *
 *  For an example usage, see ../../core/services/interceptors/common-http-interceptor.ts
 */
@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss']
})

export class NotificationComponent implements OnInit {
    notifications: Notification[] = [];

    constructor(public notificationService: NotificationService) {
    }

    ngOnInit() {
        this.notificationService.getAlert().subscribe((alert: Notification) => {
            this.notifications = [];
            if (!alert) {
                this.notifications = [];
                return;
            }
            this.notifications.push(alert);
            setTimeout(() => {
                this.notifications = this.notifications.filter(x => x !== alert);
            }, 6000);
        });
    }

    removeNotification(notification: Notification) {
        this.notifications = this.notifications.filter(x => x !== notification);
    }

    /**Set css class for Alert -- Called from alert component**/
    cssClass(notification: Notification) {
        if (!notification) {
            return;
        }
        switch (notification.type) {
            case NotificationType.Success:
                return 'toast-success';
            case NotificationType.Error:
                return 'toast-error';
            case NotificationType.Info:
                return 'toast-info';
            case NotificationType.Warning:
                return 'toast-warning';
        }
    }
}
