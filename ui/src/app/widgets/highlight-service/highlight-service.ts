import { Injectable, Inject, ElementRef } from '@angular/core';
import { PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import 'clipboard';

import 'prismjs';
import 'prismjs/plugins/toolbar/prism-toolbar';
import 'prismjs/plugins/copy-to-clipboard/prism-copy-to-clipboard';
import 'prismjs/components/prism-css';
import 'prismjs/components/prism-javascript';
import 'prismjs/components/prism-java';
import 'prismjs/components/prism-markup';
import 'prismjs/components/prism-typescript';
import 'prismjs/components/prism-sass';
import 'prismjs/components/prism-scss';
import 'prismjs/components/prism-yaml';

declare var Prism: any;

@Injectable({
  providedIn: 'root'
})
export class HighlightService {
  // TODO:
  // Clean this up
  // Make a component 
  //   - Was having issues with Prism.highlightElement working with a custom component

  constructor(@Inject(PLATFORM_ID) private platformId: Object) { }

  highlightElement(element: ElementRef) {
    if (isPlatformBrowser(this.platformId)) {
      Prism.highlightElement(element.nativeElement);
    }
  }
  highlightAll() {
    if (isPlatformBrowser(this.platformId)) {
      Prism.highlightAll();
    }
  }

}