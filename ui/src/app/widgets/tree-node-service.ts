import {Injectable} from '@angular/core';
import {BehaviorSubject, merge, Observable} from 'rxjs';
import {FlatTreeControl} from '@angular/cdk/tree';
import {CollectionViewer, SelectionChange} from '@angular/cdk/collections';
import {map} from 'rxjs/operators';

@Injectable()
export class TreeNodeService {

    dataChange = new BehaviorSubject<TreeNode[]>([]);

    get data(): TreeNode[] { return this.dataChange.value; }
    set data(value: TreeNode[]) {
        this._treeControl.dataNodes = value;
        this.dataChange.next(value);
    }

    constructor(private _treeControl: FlatTreeControl<TreeNode>,
                private dataService: any,
                private getChildren: (node: TreeNode, dataService: any) => Observable<TreeNode[]>) {}

    connect(collectionViewer: CollectionViewer): Observable<TreeNode[]> {
        this._treeControl.expansionModel.onChange.subscribe(change => {
            if ((change as SelectionChange<TreeNode>).added ||
                (change as SelectionChange<TreeNode>).removed) {
                this.handleTreeControl(change as SelectionChange<TreeNode>);
            }
        });

        return merge(collectionViewer.viewChange, this.dataChange).pipe(map(() => this.data));
    }

    /** Handle expand/collapse behaviors */
    handleTreeControl(change: SelectionChange<TreeNode>) {
        if (change.added) {
            change.added.forEach(node => this.toggleNode(node, true));
        }
        if (change.removed) {
            change.removed.slice().reverse().forEach(node => this.toggleNode(node, false));
        }
    }

    /**
     * Toggle the node, remove from display list
     */
    toggleNode(node: TreeNode, expand: boolean) {

        const index = this.data.indexOf(node);
        if (! node.isExpandable || index < 0) { // If no children, or cannot find the node, no op
            return;
        }

        node.isLoading = true;

        this.getChildren(node, this.dataService).subscribe(
            children => {
                if (expand) {
                    const nodes = children.map(child =>
                        new TreeNode(child.data, child.currentPath, child.isExpandable, node.level + 1));
                    this.data.splice(index + 1, 0, ...nodes);
                } else {
                    let count = 0;
                    for (let i = index + 1; i < this.data.length
                    && this.data[i].level > node.level; i++, count++) {}
                    this.data.splice(index + 1, count);
                }

                // notify the change
                this.dataChange.next(this.data);
                node.isLoading = false;
            }
        );
    }
}

export class TreeNode {
    constructor(public data: any, public currentPath: string, public isExpandable: boolean, public level = 1,
                public isLoading = false) {}
}
