export class ErrorResponse {
    constructor(public message: string) {
    }
}

export class SuccessResponse {
    status?: string;
    data: any;

    constructor(data: any, status?: string) {
        this.data = data;
        this.status = status;
    }
}

export const successResp = new SuccessResponse([], 'Success');

export const internalServerErrorResp = new ErrorResponse('Internal Server Error');
export const invalidInputResp = new ErrorResponse('Invalid Input');
export const jsonParseErrorResp = new ErrorResponse('Invalid JSON Input');
