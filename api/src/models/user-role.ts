export interface UserDetail {
    username: string,
    roles: string[]
}
