import {ldapConnector} from '../../service/ldap_service';
import {user_roles} from '../../constants';
import * as request from 'web-request';

jest.mock('web-request');

describe('LDAPConnector', () => {
    let ldapConnectorSpy;
    const userid = 'user';
    const password = 'password';

    function successfulSetup() {
        setupHawkeyeResp(200);
    }

    function setupHawkeyeResp(statusCode: number) {
        ldapConnectorSpy = jest.spyOn(request, 'post').mockImplementation(() => {
            return Promise.resolve({
                statusCode: statusCode
            } as any);
        });
    }

    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('Return UserDetails for authenticated and authorised users', (done) => {
        successfulSetup();
        ldapConnector.authenticate(userid, password).subscribe(
            result => {
                expect(result.username).toEqual(userid);
                expect(result.roles).toContain(user_roles.default_role);
                done();
            }, error => done.fail()
        );
    });

    it('Return 401 status error for incorrect credentials', (done) => {
        setupHawkeyeResp(401);

        ldapConnector.authenticate(userid, password).subscribe(
            result => done.fail(),
            (error: any) => {
                expect(error.statusCode).toEqual(401);
                done();
            }
        );
    });

    it('Return 500 status error for failed Hawkeye Error', (done) => {
        setupHawkeyeResp(500);

        ldapConnector.authenticate(userid, password).subscribe(
            result => done.fail(),
            (error: any) => {
                expect(error.statusCode).toEqual(500);
                done();
            }
        );
    });
});
