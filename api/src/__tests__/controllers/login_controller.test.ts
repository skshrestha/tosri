import {login, logout, rateLimiter} from '../../controllers';
import {ldapConnector} from '../../service/ldap_service';
import {of, throwError} from 'rxjs';
import {user_roles} from '../../constants';
import {mockReq} from 'sinon-express-mock';
import {Response} from 'express';
import {RateLimiterRes} from 'rate-limiter-flexible';

const MockExpress = require('mock-express');

jest.mock('../../service/ldap_service');
jest.mock( '../../middleware/authentication');
jest.mock('../../service/jwt_service');


describe('login', () => {
    let app: any;
    let ldapConnectorSpy;
    let loginLimiterSpy;
    const body = {
        body: {
            username: 'user',
            password: 'password'
        }
    };

    function setupLoginLimiter() {
        loginLimiterSpy = jest.spyOn(rateLimiter, 'consume').mockImplementation(() => Promise.resolve({} as RateLimiterRes));
    }

    function lockoutLoginLimiter() {
        loginLimiterSpy = jest.spyOn(rateLimiter, 'get').mockImplementation(() =>
            Promise.resolve({remainingPoints: -1, msBeforeNext: 10000} as RateLimiterRes))
    }

    function setupLDAP(authenticated: boolean, statusCode: number = 500) {
        ldapConnectorSpy = jest.spyOn(ldapConnector, 'authenticate').mockImplementation(() => {
            if (authenticated) {
                return of({
                    username: 'user',
                    roles: [user_roles.default_role]
                });
            } else {
                return throwError({statusCode: statusCode, message: 'Internal Server Error'});
            }
        });
    }

    function successfulSetup() {
        setupLDAP(true);

    }

    beforeEach(() => {
        app = MockExpress();
        jest.clearAllMocks();
        app.post('/login', login);
    });

    it('Successful Login should return 200 status', (done) => {
        successfulSetup();
        const req = mockReq(body);
        const resp: Response = app.makeResponse(function(err: any, res: any) {
            expect(res.status).toEqual(200);
            expect(err).toEqual(null);
            done();
        });
        resp.locals = {};
        resp.clearCookie = () => { return resp; };
        app.invoke('post', '/login', req, resp);
    });

    it('Incorrect Login should return 401 status', (done) => {
        setupLDAP(false, 401);
        setupLoginLimiter();
        const req = mockReq(body);
        const resp: Response = app.makeResponse(function(err: any, res: any) {
            expect(res.status).toEqual(401);
            expect(err).toEqual(null);
            done();
        });
        resp.locals = {};
        resp.clearCookie = () => { return resp; };
        app.invoke('post', '/login', req, resp);
    });

    it('After too many failed logins, login should return 429 status', (done) => {
        setupLDAP(false, 401);
        lockoutLoginLimiter();

        const req = mockReq(body);
        const resp: Response = app.makeResponse(function(err: any, res: any) {
            expect(res.status).toEqual(429);
            expect(err).toEqual(null);
            done();
        });
        resp.locals = {};
        resp.clearCookie = () => { return resp; };
        app.invoke('post', '/login', req, resp);
    });

    it('Internal Server Error should return 500 status', (done) => {
        setupLDAP(false);


        const req = mockReq(body);
        const resp = app.makeResponse(function(err: any, res: any) {
            expect(res.status).toEqual(500);
            expect(err).toEqual(null);
            done();
        });
        resp.locals = {};
        resp.clearCookie = () => { return resp; };
        app.invoke('post', '/login', req, resp);
    });

    describe('logout', () => {
        beforeEach(() => {
            app = MockExpress();
            jest.clearAllMocks();
            app.get('/logout', logout);
        });

        it('Successful logout', (done) => {
            const req = mockReq();
            const resp = app.makeResponse(function(err: any, res: any) {
                expect(res.status).toEqual(200);
                expect(err).toEqual(null);
                done();
            });
            app.invoke('get', '/logout', req, resp);
        });
    });
});
