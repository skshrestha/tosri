import {config} from './constants';

interface TosriComponentPluginConfig {
    tosriApi: {
        host: string,
        streamPath: string
    }
}

export const tosriComponentPluginConfig: TosriComponentPluginConfig = config.app_config['tosri'];
