import {existsSync, readFileSync} from 'fs';
import {createMessage, logger} from '../log_service';
import {Reader} from 'properties-reader';

const crypto = require('crypto');
const  PropertiesReader = require('properties-reader');

const UTF8_ENCODING = 'utf8';
const HEX_ENCODING = 'hex';
const ENCRYPTION_PREFIX = 'enc:';

const KEY_PATH = process.env.KEY_PATH || '';
const PASS_PATH = process.env.PASS_PATH || '';
class SecretService {

    private readonly algorithmMap: Map<string, string> = new Map([
        ['aes', 'aes']
    ]);
    private readonly secrets?: Reader;

    constructor() {
        if (existsSync(KEY_PATH) && existsSync(PASS_PATH)) {
            this.secrets = PropertiesReader().read(this.decryptFromFile());
        } else {
            logger.warn(createMessage('secret-service',
                `Missing secrets keyPath or passPath: ${KEY_PATH} / ${PASS_PATH}`));
        }
    }

    getSecret(key: string): string {
        if (! this.secrets ) {
            logger.info(createMessage('secret-service', `Attempted to get secret for ${key} when keyPath or passPath doesn't exist`));
            throw new Error(`Attempted to get secret for ${key} when keyPath or passPath doesn't exist`);
        }
        if (this.secrets.get(key)) {
            return `${this.secrets.get(key)}`;
        } else {
            logger.error(createMessage('secret-service', `Secret for key ${key} is not defined!`));
            throw new Error(`Secret for key ${key} is not defined!`);
        }
    }

    private decryptFromFile() {
        const encrypted = readFileSync(PASS_PATH, UTF8_ENCODING);
        if (encrypted && encrypted.startsWith(ENCRYPTION_PREFIX)) {

            const decryptionProperties = PropertiesReader(KEY_PATH);
            const cipher = this.transformToCipher(decryptionProperties.get('cipher'), decryptionProperties.get('keylen'));
            // const encryptionParam = new DecryptionProps(decryptionProperties.get('key'), decryptionProperties.get('iv'), cipher);

            const decipher = crypto.createDecipheriv(cipher, decryptionProperties.get('key'), decryptionProperties.get('iv'), {});
            let decrypted = decipher.update(encrypted.substring(ENCRYPTION_PREFIX.length), HEX_ENCODING, UTF8_ENCODING);
            decrypted += decipher.final(UTF8_ENCODING);
            return decrypted;
        } else {
            return encrypted;
        }
    }

    private transformToCipher(cipher: string, keyLength: number) {
        const cipherBlocks: string[] = cipher.split('/');
        const algorithm = this.algorithmMap.get(cipherBlocks[0].toLowerCase());
        const mode = cipherBlocks[1].toLowerCase();
        const bits = keyLength * 8;

        if (! algorithm) {
            logger.error(createMessage('secret-service', `Unknown algorithm ${algorithm}`));
            throw new Error(`Invalid algorithm was provided : ${algorithm}`);
        }

        return `${algorithm}-${bits}-${mode}`;
    }
}

export const secretService: SecretService = new SecretService();
