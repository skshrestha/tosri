import jwt from 'jsonwebtoken';
import {authTokenName, config} from '../constants';
import {createMessage, LogCategory, logger} from './log_service';
import {UserDetail} from "../models/user-role";
const cookie = require('cookie');

// The token stuff below is added only to demo FE integration, when replaced with proper solution, FE needs to
// be updated accordingly but the general auth flow will still remain the same

const secret = config.tokens.secret;

export function createJWToken(details: UserDetail): string  {
    return jwt.sign(details, secret, {expiresIn: `${config.tokens.expiryMs}`, algorithm: 'HS256'});
}

export function verifyCookie(cookiesString: any): Promise<UserDetail> {
    return new Promise((resolve, reject) => {
        if (! cookiesString) {
            reject('Cannot verify user role as cookies are undefined');
            return;
        }

        const cookies = cookie.parse(cookiesString);
        const token = cookies[authTokenName];

        resolve(verifyToken(token));
    });
}

export function verifyToken(token: any): Promise<UserDetail> {
    return new Promise((resolve, reject) => {
        if (! token) {
            reject('Cannot verify user role as jwt token is missing from the header');
            return;
        }
        jwt.verify(token, secret, (err: any, decoded: any) => {
            err && reject(err);
            if (!err) {
                const userDetail: UserDetail = decoded as UserDetail;
                logger.info(createMessage(LogCategory.access, `Verified ${userDetail.username} from jwt token`));
                if (userDetail.roles.length === 0) {
                    reject(`User ${userDetail.username} has no valid user roles this request`);
                } else {
                    resolve(userDetail);
                }
            }
        });
    });
}
