import {createLogger, format, transports} from 'winston';

const enumerateErrorFormat = format((info: any) => {
    if (info.message instanceof Error) {
        info.message = Object.assign({
            message: info.message.message,
            stack: info.message.stack
        }, info.message);
    }

    if (info instanceof Error) {
        return Object.assign({
            message: info.message,
            stack: info.stack
        }, info);
    }

    return info;
});

export const logger = createLogger({
    level: 'info',
    format: format.combine(
        enumerateErrorFormat(),
        format.timestamp(),
        format.json()
    ),
    transports: [
        new transports.Console({format: format.timestamp()})
    ]
});

export function createMessage(category: string, message: string, user?: string): any {
    return new Message(category, message, user);
}

class Message {
    category: string;
    message: any;
    user?: string;

    constructor(category: string, message: any, user?: string) {
        this.category = category;
        this.message = message;
        this.user = user;
    }
}

export function logResponseTime(method: string, user: string, uri: string, timeTaken: string, statusCode: number, statusMessage?: string) {
    const message = {
        method: method,
            statusCode: statusCode,
            uri: uri,
            timeTaken: timeTaken,
            message: statusMessage
    };
    return new Message(LogCategory.requestTimer, message, user);
}

export enum LogCategory {
    init = 'INIT',
    access = 'ACCESS',
    unknown = 'UNKNOWN',
    requestTimer = 'REQUEST_TIMER'
}
