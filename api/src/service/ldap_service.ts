import * as request from 'web-request';
import {RequestOptions, Response} from 'web-request';
import {config, user_roles} from '../constants';
import {Observable, of} from 'rxjs';
import {fromPromise} from 'rxjs/internal-compatibility';
import {createMessage, LogCategory, logger} from './log_service';
import {map} from 'rxjs/operators';
import {UserDetail} from "../models/user-role";

interface AuthenticationService {

    authenticate(username: string, password: string, restEndpoint?: boolean): Observable<UserDetail>;

}

class LDAPService implements  AuthenticationService {

    constructor(readonly uri: string, readonly mock: boolean) {}

    authenticate(username: string, password: string): Observable<UserDetail> {

        logger.info(createMessage(LogCategory.access, `Verifying credentials of ${username}`));

        return (this.mock ? this.mockAuthentication(username, password) : this.checkAuthentication(username, password)).pipe(
            map(authResponse => {
                if (authResponse.statusCode === 200) {
                    const userDetail: UserDetail =  {username: username, roles: [user_roles.default_role]};
                    if (userDetail.roles.length === 0) {
                        throw {statusCode: 403, message: 'Unauthorised Access'};
                    }
                    logger.info(createMessage(LogCategory.access,
                        `Successfully authenticated user ${username} with roles: ${userDetail.roles.join(', ')}`));
                    return userDetail;
                } else if (authResponse.statusCode === 401) {
                    throw {statusCode: 401, message: 'Invalid username and/or password'};
                } else {
                    throw {statusCode: 500, message: 'Internal Server Error'};
                }
            })
        );
    }

    private checkAuthentication(username: string, password: string): Observable<Response<string>> {
        const requestBody: {username: string, password: string} = {
            username: username,
            password: password
        };

        const options: RequestOptions = {
            method: 'POST',
            body: requestBody,
            json: true,
            strictSSL: config.authenticate.sslEnabled
        };

        return fromPromise(request.post(this.uri, options));
    }

    private mockAuthentication(username: string, password: string): Observable<{statusCode: number}> {
        if (username === 'user' && password === 'password') {
            return of({statusCode: 200});
        }
        return of({statusCode: 401});
    }
}

const authService: AuthenticationService = new LDAPService(config.authenticate.endpoint, config.authenticate.mock);
export { authService as ldapConnector };
