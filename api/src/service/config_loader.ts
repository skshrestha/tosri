import {createMessage, LogCategory, logger} from './log_service';
import {secretService} from './credentials/secret_service';
const yaml = require('merge-yaml');
const fs = require('fs');

export class ConfigLoader {

    public static loadConfig() {
        logger.info(createMessage(LogCategory.init, 'Loading config files'));

        const configFiles = ['config.yml',  'config.plugins.yml'];
        const overrideFile = '/etc/config/config_override.yml';
        try {
            fs.accessSync(overrideFile);
            logger.info(createMessage(LogCategory.init, 'Merging config.yml with config_override.yml...'));
            configFiles.push(overrideFile);
        } catch (err) {
            logger.info(createMessage(LogCategory.init, 'config_override.yml does not exist.o Using only default config values...'));
        }
        const mergedFilesString = JSON.stringify(yaml(configFiles));
        const envSubbedString = this.substituteWithEnvVars(mergedFilesString);
        const secretSubbedString = this.substitueWithSecrets(envSubbedString);

        return JSON.parse(secretSubbedString);
    }

    private static substituteWithEnvVars(content: string) {
        return content.replace(/\${([^}]*)}/g, (substring: string, ...args: any[]) => {
            return process.env[args[0]] as string;
        });
    }

    private static substitueWithSecrets(content: string) {
        return content.replace(/fs:([a-zA-Z_\-0-9]+)/g, (substring: string, ...args: any[]) => {
            return secretService.getSecret(args[0]);
        });
    }

}
