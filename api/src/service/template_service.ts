import {render} from 'ejs';
import {logger} from './log_service';

export class TemplateService {
    static readonly logCategory: string = 'templating';

    static templateString(templateString: string, templateValues: {}) {
        return render(templateString, templateValues);
    }

    static templateValues(templateString: string, values: any = {}, preValueMappings: any = {}): any {
        const valueMapper: any = {};
        Object.keys(values).forEach((key: string) => {
            if (Object.keys(preValueMappings).includes(key)) {
                const mapping = preValueMappings[key];

                if (! mapping[values[key]]) {
                    logger.error(this.logCategory, `Invalid key was provided ${values[key]} for ${JSON.stringify(mapping)}`);
                    throw new Error(`Invalid key was provided ${values[key]} for ${JSON.stringify(mapping)}`);
                }
                const matchingMap = mapping[values[key]];

                Object.keys(matchingMap).forEach(mapKey => {
                    valueMapper[mapKey] = matchingMap[mapKey];
                });
            } else {
                valueMapper[key] = values[key];
            }
        });
        return this.templateString(templateString, valueMapper);
    }
}
