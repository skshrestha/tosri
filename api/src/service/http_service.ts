import {Observable} from 'rxjs';
import {fromPromise} from 'rxjs/internal-compatibility';
import * as request from 'web-request';
import {map} from 'rxjs/operators';
import {createMessage, logger} from './log_service';

export class HttpResponse<T> {
    constructor(public statusCode: number, public statusMessage: string, public body: T, public headers: {[key: string]: any}) {}
}

export interface HttpOptions {
    headers?: {[key: string]: any};
    formData?: any;
    strictSSL?: boolean;
    maxRedirects?: number;
}

export interface HttpServiceIfc {
    get<T>(path: string, httpOptions?: HttpOptions, json?: boolean): Observable<HttpResponse<T>>;
    post<T>(path: string, body?: any, httpOptions?: HttpOptions, json?: boolean): Observable<HttpResponse<T>>;
}

const logCategory = 'outgoing-request';

export class HttpService implements HttpServiceIfc {

    constructor(private baseUrl: string, private username?: string, private password?: string) {}

    public get<T>(path: string, httpOptions: HttpOptions = {}, json: boolean = true): Observable<HttpResponse<T>> {
        const requestOptions = this.fillRequestOptions(httpOptions, json);

        logger.info(createMessage(logCategory, `Sending GET request to ${this.baseUrl}${path}`));
        return this.mapResponse(path, fromPromise(request.get(`${this.baseUrl}${path}`, requestOptions)));
    }

    public post<T>(path: string, body?: any, httpOptions?: HttpOptions, json: boolean = true): Observable<HttpResponse<T>> {
        const requestOptions = this.fillRequestOptions(httpOptions, json);
        if (body) {
            requestOptions.body = body;
        }
        logger.info(createMessage(logCategory, `Sending POST request to ${this.baseUrl}${path}`));

        return this.mapResponse(path, fromPromise(request.post(`${this.baseUrl}${path}`, requestOptions)));
    }

    private mapResponse(path: string, requestObservable: Observable<request.Response<any>>): Observable<HttpResponse<any>> {
        return requestObservable.pipe(
            map((response: request.Response<string>) => {
                logger.info(createMessage(logCategory, `Received ${response.statusCode} from ${this.baseUrl}${path}`));

                if (response.statusCode >= 400) {
                    logger.error(createMessage(logCategory,
                        `Error from ${this.baseUrl}${path} - ${response.statusMessage} - ${JSON.stringify(response.content)}`));
                    throw new HttpResponse<any>(response.statusCode, response.statusMessage, response.content, response.headers);
                }
                return new HttpResponse<any>(response.statusCode, response.statusMessage,
                    response.content, response.headers);
            })
        );
    }

    private fillRequestOptions( httpOptions: HttpOptions = {}, json: boolean = true): request.RequestOptions {
        const requestOptions: request.RequestOptions = (httpOptions || {}) as request.RequestOptions;
        if (this.username && this.password) {
            requestOptions.auth = {
                username: this.username,
                password: this.password
            };
        }
        requestOptions.json = json;
        return requestOptions;
    }
}


