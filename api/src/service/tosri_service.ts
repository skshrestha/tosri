import {Observable} from "rxjs";
import {HttpResponse, HttpService} from "./http_service";
import {map} from "rxjs/operators";
import {createMessage, logger} from "./log_service";
import {tosriComponentPluginConfig} from "../tosri-config";

export interface TosriService {
    getStreams(): Observable<any>;
}

class TosriApiService implements TosriService {
    private httpService: HttpService;

    constructor(public config: { host: string,
        streamPath: string }) {
        this.httpService = new HttpService(this.config.host);
    }

    getStreams(): Observable<any> {
        return this.httpService.get(this.config.streamPath).pipe(
            map((result: HttpResponse<any>) => {
                if (result.statusCode === 200) {
                    return result.body;
                } else {
                    logger.error(createMessage('get-streams', result.statusMessage));
                    throw new Error("Failed to get streams results");
                }
            })
        );
    }


}

export const tosriService: TosriService = new TosriApiService(tosriComponentPluginConfig.tosriApi);