import express, {Request, Response, Router} from 'express';
import * as path from 'path';
import {CorsMiddleware} from './middleware';
import {createMessage, LogCategory, logger} from './service/log_service';
import {HeartbeatController, LoginController} from './controllers';
import {authenticate} from './middleware/authentication';
import {ErrorResponse, internalServerErrorResp} from './models/response';
import {jsonErrorHandler, jsonParser} from './middleware/json_parser';
import {requestTimer} from './middleware/request_timer';
import {config} from './constants';
import {ConfigController} from './controllers/config_controller';

const pluginMiddlewares: Map<string, any> = new Map();

pluginMiddlewares.set("tosri", require(`./middleware/tosri_middleware`));

const app: express.Application = express();
const port = Number(config.port || '8080');

function unknownEndpointInterceptor(req: Request, res: Response, next: any) {

    if (! res.headersSent) {
        const errMsg = `Unknown endpoint ${req.method}: ${req.originalUrl}`;
        logger.error(createMessage(LogCategory.unknown, errMsg));
        res.status(404).json(new ErrorResponse(errMsg));
    }
    next();
}

function unknownErrorInterceptor(err: Error, req: Request, res: Response, next: any) {
    logger.error(createMessage(LogCategory.unknown, err.message));
    res.status(500).json(internalServerErrorResp);
    next();
}

app.disable('x-powered-by');
app.use(CorsMiddleware);
app.use('/', express.static('public'));
app.use(jsonParser);
app.use(jsonErrorHandler);
app.use(HeartbeatController);

const apiRouter = Router();
apiRouter.use('/auth', LoginController, unknownErrorInterceptor, unknownEndpointInterceptor, requestTimer);
apiRouter.use('/config', ConfigController, unknownErrorInterceptor, unknownEndpointInterceptor, requestTimer);

pluginMiddlewares.forEach((pluginMiddleware, plugin) => {
    const pluginRouter = Router();

    if (pluginMiddleware.noAuthRouter) {
        logger.info(createMessage(LogCategory.init, `Registered ${plugin} plugin's noAuthRouter middleware which does not require login.`));
        pluginRouter.use(pluginMiddleware.noAuthRouter);
    } else {
        logger.info(createMessage(LogCategory.init, `The ${plugin} plugin does not have a noAuthRouter middleware.`));
    }

    if (pluginMiddleware.withAuthRouter) {
        logger.info(createMessage(LogCategory.init, `Registered ${plugin} plugin's withAuthRouter middleware which requires login.`));
        pluginRouter.use(authenticate, pluginMiddleware.withAuthRouter);
    } else {
        logger.info(createMessage(LogCategory.init, 'The plugin does not have a withAuthRouter middleware.'));
    }
    apiRouter.use('/' + plugin, pluginRouter, unknownErrorInterceptor, unknownEndpointInterceptor, requestTimer);
});

app.use('/api', apiRouter, unknownEndpointInterceptor, unknownErrorInterceptor, requestTimer);

// Fallback page for html 5 history api based routing in the client side ..
app.get('*', (req, res) => {
    logger.info(createMessage(LogCategory.init, req.url));
    const filename = req.url;
    if (filename.endsWith('.js')) {
        res.sendFile(path.resolve(__dirname, 'public', filename.replace('/', '')));
    } else {
        res.sendFile(path.resolve(__dirname, 'public', 'index.html'));
    }
});

app.listen(port, () => {
    logger.info(createMessage(LogCategory.init, `Listening at http://localhost:${port}/`));
});

