import {ConfigLoader} from './service/config_loader';

export const config: {
    appName: string
    app_config: any;
    clientEndpoint: string
    authenticate: {
        endpoint: string
        sslEnabled: boolean
        mock: boolean
    },
    tokens: {
        secret: string,
        httpOnly: boolean,
        secure: boolean
        expiryMs: number
    },
    login: {
        freeRetries: number,
        accountLockoutSec: number,
        retryLifetimeSec: number
    }
    port: number
} = ConfigLoader.loadConfig();

export const allowedOrigin = process.env.ALLOWED_ORIGIN || config.clientEndpoint;

export const user_roles: { default_role: string, admin: string, app: string } = {
    default_role: 'user',
    admin: 'admin',
    app: 'app'
};

export const authTokenName = config.appName.toLowerCase().replace(/\s+/, '-') + '-token';
