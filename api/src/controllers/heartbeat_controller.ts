import {NextFunction, Request, Response, Router} from 'express';
import {SuccessResponse} from '../models/response';

const router: Router = Router();

router.get('/healthcheck', (req: Request, res: Response, next: NextFunction) => {
    res.status(200).json(new SuccessResponse('OK', '200'));
});


export const HeartbeatController: Router = router;
