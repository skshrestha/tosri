import {CookieOptions, Request, Response, Router} from 'express';
import {createJWToken} from '../service/jwt_service';
import {ldapConnector} from '../service/ldap_service';
import {allowedOrigin, authTokenName, config} from '../constants';
import {createMessage, LogCategory, logger} from '../service/log_service';
import {ErrorResponse, internalServerErrorResp, successResp, SuccessResponse} from '../models/response';
import {RateLimiterMemory} from 'rate-limiter-flexible';
import {fromPromise} from 'rxjs/internal-compatibility';

const router: Router = Router();

interface AuthRequest {
    username: string;
    password: string;
}

export const rateLimiter = new RateLimiterMemory({
    points: config.login.freeRetries,
    keyPrefix: 'login_fail',
    duration: config.login.retryLifetimeSec,
    blockDuration: config.login.accountLockoutSec
});

export const login = (req: Request, res: Response, next: any) => {
    logger.info(createMessage(LogCategory.access, 'POST: /login'));
    const userInfo: AuthRequest = req.body;

    ldapConnector.authenticate(userInfo.username, userInfo.password).subscribe(
        userDetail => {
            res.locals.userDetails = userDetail;
            const cookie = createJWToken(userDetail);
            const response = {
                username: userDetail.username,
                roles: userDetail.roles,
                expiryMs: new Date().getTime() + config.tokens.expiryMs
            };

            res.status(200)
                .cookie(authTokenName, cookie,
                    {httpOnly: config.tokens.httpOnly, domain: allowedOrigin,
                        secure: config.tokens.secure, maxAge: config.tokens.expiryMs} as CookieOptions)
                .json(new SuccessResponse(response));

            rateLimiter.delete(userDetail.username).then(result => {
                next();
            }).catch(error => {
                next();
            });
        },
        error => {
            logger.warn(createMessage(LogCategory.access, `Failed to log in ${userInfo.username} due to error ${error.message}`));
            const statusCode = error.statusCode ? error.statusCode : 500;
            limitFailedLogin(statusCode, userInfo.username, res, next);
        }
    );
};

function limitFailedLogin(statusCode: number, username: string, res: Response, next: any) {
    if (statusCode === 401) {
        const usernameLimit = rateLimiter.get(username);

        if (usernameLimit !== null) {
            fromPromise(usernameLimit).subscribe(
                usernameLimitRes => {
                    const retrySecs = (usernameLimitRes !== null && usernameLimitRes.remainingPoints <= 0) ?
                        Math.round(usernameLimitRes.msBeforeNext / 1000) : 0;

                    if (retrySecs > 0) {
                        res.status(429).set('Retry-After', String(retrySecs))
                            .json(new ErrorResponse(`Too many requests - the account will be locked for ${Math.round(retrySecs / 60)} minutes`));
                        next();
                    } else {
                        rateLimiter.consume(username).then(result => {
                            res.status(401)
                                .clearCookie(authTokenName)
                                .json(new ErrorResponse('Incorrect username and/or password'));
                            next();
                        }).catch(error => {
                            console.error(error);
                            res.status(429)
                                .set('Retry-After', String(retrySecs))
                                .json(new ErrorResponse('Too many requests - the account will be locked for 60 minutes'));
                            next();
                        })
                    }
                },
                error => {
                    logger.error(createMessage(LogCategory.access, error));
                    res.status(401)
                        .clearCookie(authTokenName)
                        .json(new ErrorResponse('Incorrect username and/or password'));
                    next();
                }
            )
        } else {
            rateLimiter.consume(username).then(result => {
                res.status(401)
                    .clearCookie(authTokenName)
                    .json(new ErrorResponse('Incorrect username and/or password'));
                next();
            }).catch(error => {
                res.status(error.statusCode)
                    .clearCookie(authTokenName)
                    .json(new ErrorResponse(error.message));
                next();
            });
        }
    } else {
        res.status(500)
            .json(internalServerErrorResp);
        next();
    }
}

export const logout = (req: Request, res: Response, next: any) => {
    logger.info(createMessage(LogCategory.access, 'GET: /logout'));
    res.status(200).clearCookie(authTokenName).json(successResp);
    next();
};

router.post('/login', login);

router.get('/logout', logout);

export const LoginController: Router = router;
