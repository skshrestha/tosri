import {Request, Response, Router} from 'express';
import {config} from '../constants';
import {ErrorResponse, SuccessResponse} from '../models/response';

const router: Router = Router();
export const getRequestDefinition = (req: Request, res: Response, next: any) => {
    const plugin = req.params.plugin;
    const requestName = req.params.requestName;
    const pluginConfig = config.app_config[plugin];

    if (pluginConfig && pluginConfig.requestDefinitions && pluginConfig.requestDefinitions[requestName]) {
        res.status(200).json(new SuccessResponse(pluginConfig.requestDefinitions[requestName]));
    } else {
        res.status(404).json(new ErrorResponse(`${plugin} plugin has no definition for request ${requestName}`));
    }
    next();
};

router.get('/request-definitions/:plugin/:requestName', getRequestDefinition);

export const ConfigController: Router = router;
