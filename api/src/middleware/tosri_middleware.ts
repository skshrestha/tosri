import {NextFunction, Request, Response, Router} from 'express';
import {internalServerErrorResp, SuccessResponse} from '../models/response';
import {tosriService} from "../service/tosri_service";
import {createMessage, logger} from "../service/log_service";

/**
 *  middleware.ts file is required to define the routers that will be used as middleware to intercept and process your requests to & from your plugin API.
 *  The paths are automatically prefixed with /api/<your_plugiun_name>
 *      e.g. router.get('/history',....) will be accessible via http://localhost:8080/api/<your_plugin_name>/history
 */

/**
 *  Export Router with routes that don't require authentication as noAuthRouter
 */
export const noAuthRouter: Router = Router();
noAuthRouter.get('/streams', (req: Request, res: Response, next: NextFunction) => {
    tosriService.getStreams().subscribe(
        (result: any) => {
            res.status(200).json(new SuccessResponse(result));
        },
        error => {
            logger.error(createMessage('get-streams-results', 'Failed to get job streams'));
            logger.error(error);
            res.status(500).json(internalServerErrorResp);
        }
    )
});


/**
 *  Export Router with routes that requiring authentication as withAuthRouter
 */
export const withAuthRouter = Router();
withAuthRouter.post('/query', (req: Request, res: Response, next: NextFunction) => {
    res.status(200).json(new SuccessResponse('Implement me!'));
    next();
});

