import bodyParser from 'body-parser';
import {Request, Response} from 'express';
import {jsonParseErrorResp} from '../models/response';

export const jsonParser = bodyParser.json({type: 'application/json'});

export const jsonErrorHandler = function(err: Error, req: Request, res: Response, next: any) {
    res.status(400).json(jsonParseErrorResp);
};
