import {NextFunction, RequestHandler, Request, Response} from 'express';
import {allowedOrigin} from '../constants';

const middleware: RequestHandler = (req: Request, res: Response, next: NextFunction) => {
    res.locals.start = Date.now();
    res.setHeader('Access-Control-Allow-Headers', 'Authorization');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('X-Powered-By', 'Kabuki');
    res.setHeader('Cache-Control', 'no-store');
    res.setHeader('Pragma', 'no-cache');
    res.setHeader('Expires', -.1);
    res.setHeader('Strict-Transport-Security', 'max-age=31536000');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST');
    res.setHeader('Access-Control-Allow-Origin', allowedOrigin);
    next();
};

export const CorsMiddleware: RequestHandler = middleware;
