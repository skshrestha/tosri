import {Request, Response} from 'express';
import {logger, logResponseTime} from '../service/log_service';
import {UserDetail} from "../models/user-role";

export function requestTimer(req:  Request, resp: Response) {
    const userDetails: UserDetail = resp.locals.userDetails;

    const user = userDetails ? userDetails.username : 'unauthenticated user';
    const uri = req.originalUrl;
    const startTime: number = resp.locals.start;
    const timeTaken = `${Date.now() - startTime}ms`;
    const statusCode = resp.statusCode;
    const method = req.method;

    if (statusCode >= 400) {
        logger.error(logResponseTime(method, user, uri, timeTaken, statusCode, resp.statusMessage));
    } else {
        logger.info(logResponseTime(method, user, uri, timeTaken, statusCode));
    }
}
