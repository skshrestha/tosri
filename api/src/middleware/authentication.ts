import {Request, Response} from 'express';
import {createMessage, LogCategory, logger} from '../service/log_service';
import {verifyCookie} from '../service/jwt_service';
import {ErrorResponse} from '../models/response';
import {requestTimer} from './request_timer';
import {UserDetail} from "../models/user-role";


export function authenticate(req:  Request, resp: Response, next: any) {
    if (resp.headersSent) {
        next();
    } else {
        checkUserDetails(verifyCookie(req.headers.cookie), req, resp, next);
    }
}

function checkUserDetails(userDetailPromise: Promise<UserDetail>, req: Request, resp: Response, next: any) {
    userDetailPromise.then( (userDetail: UserDetail) => {
        logger.info(createMessage(LogCategory.access,
            `${req.method} ${req.path} : access attempt by ${userDetail.username} with roles ${userDetail.roles} from ${req.ip}`));

        resp.locals.userDetails = userDetail;
        next();
    }).catch(error => {
        logger.warn(createMessage(LogCategory.access,
            `${req.method} ${req.path} : Unauthenticated access attempt from ${req.ip} with error ${error}`));
        resp.status(401).json(new ErrorResponse('Unauthenticated Request'));
        requestTimer(req, resp);
    });
}
